package models

import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.GeneratedValue

/**
 * @author diogocarleto
 *
 */
@Entity
case class Person(firstname: String, surname: String) {

  def this() = this(null, null)
  
  @Id 
  @GeneratedValue 
  var id: Long = _
}