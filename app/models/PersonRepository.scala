package models

import java.lang.Long

import org.springframework.data.repository.CrudRepository

import javax.inject.Named
import javax.inject.Singleton

@Named
@Singleton
trait PersonRepository extends CrudRepository[Person, Long] {

}