package infra

import org.springframework.context.annotation.AnnotationConfigApplicationContext
import play.Logger

/**
 * @author diogocarleto
 *
 */
object SpringContextUtils {

  val ctx = new AnnotationConfigApplicationContext()
  
  def registerContext() = {
    ctx.register(classOf[SpringDataJpaConfiguration]);
    ctx.scan("models")
    ctx.refresh()
    ctx.start()
  }
  
  def deregisterContext() = {
    ctx.stop()
  }
  
  def getSpringBean[T](tClass: Class[T]) = {
    val springBean = ctx.getBean(tClass)
    Logger.debug("springBean: "+springBean)
    springBean
  }
}