package infra

import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import javax.persistence.EntityManagerFactory
import javax.persistence.Persistence
import org.springframework.context.annotation.Bean
import org.springframework.orm.hibernate3.HibernateExceptionTranslator;
import org.springframework.orm.jpa.JpaTransactionManager

/**
 * @author diogocarleto
 *
 */
@Configuration
@EnableJpaRepositories(Array("models"))
class SpringDataJpaConfiguration {
  
  final val DEFAULT_PERSISTENCE_UNIT = "default"
  
  @Bean
  def entityManagerFactory(): EntityManagerFactory = {
    Persistence.createEntityManagerFactory(DEFAULT_PERSISTENCE_UNIT)
  }
  
  @Bean
  def hibernateExceptionTranslator(): HibernateExceptionTranslator = {
    new HibernateExceptionTranslator()
  }
  
  @Bean
  def transactionManager(): JpaTransactionManager = {
    new JpaTransactionManager()
  }
}