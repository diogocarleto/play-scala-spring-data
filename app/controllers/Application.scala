package controllers

import play.api._
import play.api.mvc._
import infra.SpringContextUtils
import models.PersonRepository
import models.Person
import models.Person

object Application extends Controller {

  def index = Action {
    val personRepository = SpringContextUtils.getSpringBean(classOf[PersonRepository]);
    
    val person = Person("Bruce", "Smith")
    val savedPerson = personRepository.save(person)
    
    val retrievedPerson = personRepository.findOne(savedPerson.id)
    
    Ok(views.html.index("Your new application is ready."+retrievedPerson.id))
  }
}