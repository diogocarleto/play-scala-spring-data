import play.api._
import play.Logger
import infra.SpringContextUtils

/**
 * @author diogocarleto
 *
 */
object Global extends GlobalSettings {

  override def onStart(app: Application) {
    super.onStart(app)
    SpringContextUtils.registerContext()
  }

  override def onStop(app: Application) {
    super.onStop(app)
    SpringContextUtils.deregisterContext()
  }
  
}